package ru.kev.cryptography;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.*;

/**
 * Класс в котором реализованно дешифрование текста.
 * Текст и ключ берётся из текстовых файлов; 
 * key.txt - содержится ключ.
 * Secret.txt - содержится зашифрованное слово.
 * 
 * @author Karnauhov E.V. 
 */
public class Decryptor {
    public static void main(String [] args) {

        try(FileInputStream fis = new FileInputStream("key.txt") ) {

            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            DESedeKeySpec keyspec = new DESedeKeySpec(readKeyFromTxt(fis));

            SecretKey key = skf.generateSecret(keyspec);
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, key);
            ObjectInputStream ois = new ObjectInputStream(new CipherInputStream(new FileInputStream("Secret.txt"), cipher));
            String secret = (String) ois.readObject();

            ois.close();

            System.out.print("Дешифрование выполнено:");
            System.out.println(secret);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Метод для чтения ключа из текстового файла
     *
      * @param fis FileInputStream
     * @return возвращает массив байтов ключа
     * @throws IOException исключение
     */
    public static byte[] readKeyFromTxt(FileInputStream fis) throws IOException {
        byte[] keyspecbytes = new byte[fis.available()];
        fis.read(keyspecbytes);
        return  keyspecbytes;
    }
}
