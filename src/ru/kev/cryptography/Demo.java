package ru.kev.cryptography;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.*;
import java.util.Scanner;


/**
 * Класс в котором реализованно шифрование текста.
 * Текст вводится с клавиатуры , ключ задаётся генератором инстанса класса KeyGenerator;
 *
 * @author Karnauhov E.V.
 */

public class Demo {
    public static void main(String[] args) throws IOException {


        try(BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\HP\\IdeaProjects\\Cryptography\\Secret.txt"));
            FileOutputStream fos = new FileOutputStream("key.txt");
        ) {

            KeyGenerator kg = KeyGenerator.getInstance("DESede");
            SecretKey key = kg.generateKey();
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            ObjectOutputStream oos = new ObjectOutputStream(new CipherOutputStream(new FileOutputStream("Secret.txt"), cipher));
            oos.writeObject(write_msg());

            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            DESedeKeySpec keyspec = (DESedeKeySpec) skf.getKeySpec(key, DESedeKeySpec.class);


            fos.write(keyspec.getKey());

            oos.close();
            System.out.println("Шифрование выполнено:" + reader.readLine());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public static String write_msg() {

        System.out.println("Введите текст, для шифрования: ");
        Scanner scanner = new Scanner(System.in);

        return scanner.nextLine();
    }
}